<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    //
    protected $table = 'producto';
    protected $fillable =['nombre','codigo','precio','categorias_id'];

    public function producto()
	{
		return $this->hasMany('App\categorias::class');
	}  
}
