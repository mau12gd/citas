@extends('layouts.app')

@section('content')


<div class="form col-12" >
<h1 class="nomCliente" >Modificar Cliente</h1> 
    <form action="{{url('/cliente/'.$cliente->id) }}" method="post" enctype="multipart/form-data"> 
            <!-- token de seguridad -->
            {{ csrf_field() }}
            <!-- Tipo de solicitud | _method -->
             {{method_field('PATCH')}}
             @include ('cliente.form', ['modo'=>'editar'])
        </form>
    </div>         

@endsection
  