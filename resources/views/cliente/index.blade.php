@extends('layouts.app')       
@section('content')   
 <a href="{{asset(url('cliente/create'))}}"class="botton3  text-decoration-none" >Crear cliente</button></a>
            <table class="resp">
            <h1 class="titl">Listado de Clientes</h1>
            
                <thead >
                    <tr>  
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellido</th>
                        <th scope="col">direccion</th>
                        <th scope="col">Teléfono</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($clientes as $cliente)
                    <tr>   
                        <td class="td-c">{{$cliente->nombre}}</td>
                        <td class="td-c">{{$cliente->apellido}}</td> 
                        <td class="td-c">{{$cliente->direccion}}</td>
                        <td class="td-c">{{$cliente->telefono}}</td>
                        <td class="td-c">
                            <a href="{{url('cliente/'.$cliente->id.'/edit') }}"> 
                            <button class="botton" type="submit" id="id" name="id" >Modificar</button></a>
                           
                            <form action="{{ url('/cliente/'.$cliente->id) }}"   method="POST">
                               @method('DELETE')
                               @csrf
                            <button class="botton2" type="submit" onclick="return confirm('¿Desea Eliminar el Dato?')">Eliminar</button>
                            </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>    
        </div>
            </table>
@endsection
